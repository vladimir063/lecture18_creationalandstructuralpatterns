package org.sber.adapter;

import org.sber.card.Card;

public class Adapter implements CashMachineCardOperations {

    private Card card;

    public Adapter(Card card) {
        this.card = card;
    }

    @Override
    public void authorization() {
        System.out.println("Авторизация прошла успешно");
    }

    @Override
    public void getBalance() {
        System.out.println("Баланс вашей карты  " + card.getBalance());
    }

    @Override
    public void putMoney(long sum) {
        card.addMoney(sum);
        System.out.println("На ваш счет зачислено " + sum);

    }

    @Override
    public long withdrawMoney(long sum) {
        card.getMoney(sum);
        System.out.println("Вы успешно сняли " + sum);
        return sum;
    }

    @Override
    public void closeSession() {
        System.out.println("Сессия закрыта");
    }

}
