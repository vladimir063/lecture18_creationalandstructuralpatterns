package org.sber.adapter;

public interface CashMachineCardOperations {

    void authorization();
    void getBalance();
    void putMoney(long sum);
    long withdrawMoney(long sum);
    void closeSession();

}
