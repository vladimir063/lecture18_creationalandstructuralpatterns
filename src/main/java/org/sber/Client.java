package org.sber;

import org.sber.adapter.Adapter;
import org.sber.card.Card;
import org.sber.factory.AbstractFactoryCard;

public class Client {

    private Card card;
    private Adapter adapter;


    public Client(AbstractFactoryCard abstractFactoryCard, double balance, String cardType, String cardNumber) {
        this.card = abstractFactoryCard.createCard(balance,  cardType, cardNumber);
        this.adapter = new Adapter(card);
    }

    public void release() {
        card.release();
    }

    public Adapter execute(){
        return adapter;
    }

}
