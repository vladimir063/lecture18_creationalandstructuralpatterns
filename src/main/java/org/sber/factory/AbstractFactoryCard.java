package org.sber.factory;

import org.sber.card.Card;

public interface AbstractFactoryCard {

    Card createCard(double balance,  String cardType, String cardNumber);

}
