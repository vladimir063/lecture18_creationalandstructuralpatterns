package org.sber.factory;

import org.sber.card.Card;
import org.sber.card.CreditCard;
import org.sber.card.DebitCard;

public class MasterCardFactory implements AbstractFactoryCard {


    @Override
    public Card createCard(double balance, String cardType, String cardNumber) {
        if (cardType.equalsIgnoreCase("debit")) {
            return new DebitCard(balance, "MasterCard", cardType, cardNumber);
        } else if (cardType.equalsIgnoreCase("credit")) {
            return new CreditCard(balance, "MasterCard", cardType, cardNumber);
        }
        throw new RuntimeException("неверный тип карты");
    }
}
