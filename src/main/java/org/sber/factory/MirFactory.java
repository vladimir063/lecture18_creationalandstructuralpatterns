package org.sber.factory;

import org.sber.card.Card;
import org.sber.card.CreditCard;
import org.sber.card.DebitCard;

public class MirFactory implements AbstractFactoryCard {

    @Override
    public Card createCard(double balance, String cardType, String cardNumber) {
        if (cardType.equalsIgnoreCase("debit")) {
            return new DebitCard(balance, "MIR", cardType, cardNumber);
        } else if (cardType.equalsIgnoreCase("credit")) {
            return new CreditCard(balance, "MIR", cardType, cardNumber);
        }
        throw new RuntimeException("неверный тип карты");
    }

}
