package org.sber;

import org.sber.factory.AbstractFactoryCard;
import org.sber.factory.MirFactory;

public class Main {

    public static void main(String[] args) {
        AbstractFactoryCard abstractFactoryCard = new MirFactory();
        Client client = new Client(abstractFactoryCard, 10000, "credit", "555" );
        client.release();
        client.execute().authorization();
        client.execute().getBalance();
        client.execute().putMoney(5000);
        client.execute().withdrawMoney(1000);
        client.execute().getBalance();
        client.execute().closeSession();

    }

}
