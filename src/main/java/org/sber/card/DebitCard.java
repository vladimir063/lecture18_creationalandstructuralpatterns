package org.sber.card;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DebitCard extends Card {

    protected boolean savingsProgramBonuses;
    protected double savingsProgramBonusesAmount;

    public DebitCard(double balance, String paymentSystem, String cardType, String cardNumber) {
        super(balance, paymentSystem, cardType, cardNumber);
        this.savingsProgramBonuses = false;
        this.savingsProgramBonusesAmount = 0;
    }

    @Override
    public void release(){
        super.release();
        System.out.println("Программа накопления бонусов " + savingsProgramBonuses + "\n" +
                "количество бонусов " + savingsProgramBonusesAmount);
    }

}
