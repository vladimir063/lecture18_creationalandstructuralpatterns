package org.sber.card;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCard extends Card {

    private double interestRate;
    private double limit;

    public CreditCard(double balance, String paymentSystem, String cardType, String cardNumber) {
        super(balance, paymentSystem, cardType, cardNumber);
        this.interestRate = 14;
        this.limit = 100000;
    }

    @Override
    public void release() {
        super.release();
        System.out.println("Процентная ставка " + interestRate + "\n" +
                "кредитный лимит " + limit );
    }

}
