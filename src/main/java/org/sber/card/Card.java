package org.sber.card;



public abstract class Card {

    protected double balance;
    protected String paymentSystem;
    protected String cardType;
    protected String cardNumber;


    public Card(double balance, String paymentSystem, String cardType, String cardNumber) {
        this.balance = balance;
        this.paymentSystem = paymentSystem;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void addMoney(long sum) {
        balance += sum;
    }

    public void getMoney(long sum) {
        balance -= sum;
    }

    public  void release(){
        System.out.printf("Выпущена новая карта \n " +
                "баланс : %,.2f \n" +
                "платежная система %s \n" +
                "тип карты %s \n" +
                "номер карты %s \n", balance, paymentSystem, cardType, cardNumber);
    }

}

